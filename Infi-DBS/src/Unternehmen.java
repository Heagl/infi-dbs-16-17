import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Unternehmen {
	
	static void Database()
	{
		Connection c = null;
	    Statement stmt = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:Unternehmen.db");
	      System.out.println("Opened database successfully");

	      stmt = c.createStatement();
	      String sql = "CREATE TABLE IF NOT EXISTS Unternehmen " +
	                   "(Produkte TEXT PRIMARY KEY," +
	                   " Preis REAL    NOT NULL)"; 
	      stmt.executeUpdate(sql);
	      System.out.println("Table Unternehmen created successfully");
	      sql = "CREATE TABLE IF NOT EXISTS Fabriken " +
	              "(Ort TEXT," +
	              " Stra�e TEXT"
	              +" Teilprodukt VARCHAR(20))"; 
	      stmt.executeUpdate(sql);
	      System.out.println("Table Fabriken created successfully");
	      sql = "CREATE TABLE IF NOT EXISTS Mitarbeiter "
	              +"( Verdienst REAL,"
	              +" Name  TEXT,"
	              +" Abteilung TEXT)";
	      stmt.executeUpdate(sql);
	      System.out.println("Table Mitarbeiter created successfully");
	      sql = "CREATE TABLE IF NOT EXISTS Bestandsliste " +
	              "(Produkt1 TEXT," +
	              " Teilprodukt1 VARCHAR(20),"
	              +" FOREIGN KEY(Produkt1) REFERENCES Unternehmen(Produkte), FOREIGN KEY(Teilprodukt1) REFERENCES Fabriken(Teilprodukt))"; 
	      stmt.executeUpdate(sql);
	      System.out.println("Table Bestandsliste created successfully");
	      sql = "CREATE TABLE IF NOT EXISTS Produktionsliste " +
	              "(Teilprodukte2 VARCHAR(20)," +
	              " Abteilung1 TEXT,"
	              +" FOREIGN KEY(Teilprodukte2) REFERENCES Fabriken(Teilprodukt), FOREIGN KEY(Abteilung1) REFERENCES Mitarbeiter(Abteilung))";
	      stmt.executeUpdate(sql);
	      System.out.println("Table Produktionsliste created successfully");
	      stmt.close();
	      c.close();
	      System.out.println("Tables created successfully");
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    System.out.println("Database created successfully");

	}
	
}
