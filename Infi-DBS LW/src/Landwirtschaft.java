import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Landwirtschaft 
{
	private static Connection c;

	public static void main() 
	{
		try
		{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:landwirtschaft.db");

			//clearTables("Tiere");
			createTableTiere();
			createTableLandwirtschaft();
			createTableLandwirtschaftsadresse();
			createTableStall();
			anzahlTiere();
			insertTier("Kuh", 530.2, 2010,"Milka",001);
			insertTier("Kuh", 420.2, 2010,"Milka",002);
			insertTier("Kuh", 526.2, 2010,"Milka",003);
			insertTier("Kuh", 501.2, 2010,"Milka",004);
			insertTier("Schaf", 20, 2014,"Dolly",005);
			System.out.println("Sie haben zur Zeit "+anzahlTiere()+" Anzahl an schlachtreifen Tieren");
			
			c.close();

		}
		catch(Exception e)
		{
			System.err.println(e.getClass().getName()+": "+ e.getMessage());
			System.exit(0);
		}
	}
	public static void clearTables(String clearSql) throws SQLException
	{
		Statement stmt = c.createStatement();
		String drop = "drop table "+clearSql;
		stmt.executeUpdate(drop);
		stmt.close();
	}
	public static void insertTier(String art, double gewicht, int geburtsjahr, String name, int chipnr) throws SQLException
	{
		Statement stmt = c.createStatement();
		String sql = "INSERT INTO Tiere (art,gewicht,geburtsjahr,name,chipNr) " +
                "VALUES (\""+art+"\","+gewicht+","+geburtsjahr+",\""+name+"\","+chipnr+" );"; 
		System.out.println(sql);
		stmt.executeUpdate(sql);
		System.out.println("inserted successfully");
		stmt.close();
	}

	public static void createTableTiere() throws SQLException
	{
		Statement stmt = c.createStatement();

		String createTierTableSQL =
				"CREATE TABLE if not exists Tiere "
						+ "(art 		TEXT		NOT NULL, "
						+ "gewicht 		REAL		NOT NULL, "
						+ "geburtsjahr 	INTEGER		NOT NULL, "
						+ "name 		TEXT		NOT NULL, "
						+ "chipNr 		INTEGER 	PRIMARY KEY		NOT NULL);";

		System.out.println("created table Tiere successfully");
		stmt.executeUpdate(createTierTableSQL);
		stmt.close();

	}
	public static void createTableLandwirtschaft() throws SQLException{
		Statement stmt = c.createStatement();

		String tableLandwirtschaft="CREATE TABLE IF NOT EXISTS Landwirtschaft" 
				+"(adressenID INTEGER," 
				+"firmenname TEXT," 
				+"PRIMARY KEY(adressenID,firmenname));";

		System.out.println("created table Landwirtschaft successfully");
		stmt.executeUpdate(tableLandwirtschaft);
		stmt.close();	
	}
	public static void createTableLandwirtschaftsadresse() throws SQLException{
		Statement stmt = c.createStatement();

		String tableLandwirtschaftsadresse="CREATE TABLE IF NOT EXISTS Landwirtschaftsadresse "
				+ "(plz INTEGER,"+" ort TEXT,"+" strasseTEXT,"+" hausNr INTEGER,"
				+ "firmenname TEXT, "
				+ "adressenID_LW INTEGER,"
				+"FOREIGN KEY (adressenID_LW, firmenname) references Landwirtschaft (firmenname, adressenID)," 
				+"PRIMARY KEY (firmenname, adressenID_LW));";

		System.out.println("created table Landwirtschaftsadresse successfully");
		stmt.executeUpdate(tableLandwirtschaftsadresse);
		stmt.close();

	}
	public static void createTableStall() throws SQLException{
		Statement stmt = c.createStatement();

		String tableStall="CREATE TABLE IF NOT EXISTS Stall "
				+ "(bezeichnung TEXT,"
				+ " plaetze INTEGER,"
				+ " baujahr INTEGER,"
				+ " flaeche REAL,"
				+ " material TEXT,"
				+ " adressenID_LW INTEGER,"
				+ " firmenname TEXT,"
				+"besetztePlaetze INTEGER NULL,"
				+" FOREIGN KEY (adressenID_LW, firmenname) references Landwirtschaft(adressenID, firmenname),"
				+ "PRIMARY KEY (bezeichnung, adressenID_LW, firmenname));";


		System.out.println("created table Stall successfully");
		stmt.executeUpdate(tableStall);
		stmt.close();

	}
	public static void createTableStalltiere() throws SQLException{
		Statement stmt = c.createStatement();

		String tableStalltiere= "CREATE TABLE IF NOT EXISTS StallTiere "
				+ "(chipNr INTEGER,"
				+ " bezeichnung TEXT,"
				+ "FOREIGN KEY (chipNr) references Tiere (chipNr),"
				+ "FOREIGN KEY (bezeichnung) references Stall (bezeichnung),"
				+"PRIMARY KEY (chipNr, bezeichnung));";

		System.out.println("created table Stalltiere successfully");
		stmt.executeUpdate(tableStalltiere);
		stmt.close();
	}

	public static void createTableBesitzer() throws SQLException{
		Statement stmt = c.createStatement();

		String tableBesitzer="CREATE TABLE IF NOT EXISTS Besitzer "
				+ "(vorname TEXT, nachname TEXT,"
				+ " zweiterVorname TEXT NULL,"
				+ " adressenID INTEGER,"
				+ " adressenID_LW INTEGER,"
				+ " firmenname TEXT,"
				+"FOREIGN KEY(adressenID_LW, firmenname) references Landwirtschaft (adressenID, firmenname)," 
				+"PRIMARY KEY (vorname, nachname, adressenID_LW, firmenname, adressenID));";

		System.out.println("created table Besitzer successfully");
		stmt.executeUpdate(tableBesitzer);
		stmt.close();
	}	

	public static void createTableBesitzeradresse() throws SQLException{
		Statement stmt = c.createStatement();

		String tableBesitzeradresse="CREATE TABLE IF NOT EXISTS Besitzeradresse"
				+ " (plz INTEGER,"
				+ " ort TEXT, strasse TEXT,"
				+ " hausNr INTEGER, adressenID_B INTEGER,"
				+ " vorname TEXT, nachname TEXT,"
				+"FOREIGN KEY (adressenID_B, vorname, nachname) referneces Besitzer (adressenID, vorname, nachname),"
				+ " PRIMARY KEY (adressenID_B, vorname, nachname));";

		System.out.println("created table Besitzeradresse successfully");
		stmt.executeUpdate(tableBesitzeradresse);
		stmt.close();
	}
	public static int anzahlTiere() throws SQLException{

		Statement stmt = c.createStatement();

		ResultSet rs = stmt.executeQuery( "SELECT geburtsjahr FROM Tiere;" );
		int zaehler=0;
		while ( rs.next() ) {
			int geburtsjahr = rs.getInt("geburtsjahr");
			zaehler++;

		}

		rs.close();
		stmt.close();

		return zaehler;
	}
}	