import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Scanner;

public class WaggonBsp {

	public void WaggonBsp()
	{
		System.out.println("Please give me a Name");
		Scanner sc = new Scanner(System.in);
		String username = sc.nextLine(); 
		System.out.println("My new name will be "+username);
		
	}
	public void Waggonmain()
	{
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:Unternehmen.db");
			System.out.println("Opened database successfully");
			stmt = c.createStatement();
			String sql = "Pragma foreign_keys = on;";
			stmt.executeUpdate(sql);

			sql = 	"CREATE TABLE IF NOT EXISTS Wagon " +
					"(WagonIDW INT PRIMARY KEY," +
					" SitzplatzW INT UNIQUE NOT NULL );";
			stmt.executeUpdate(sql);

			sql = 	"CREATE TABLE IF NOT EXISTS WagonKlasse " +
					"(WagonID INT PRIMARY KEY," +
					" Klasse INT  NOT NULL,"+
					" FOREIGN KEY (WagonID) REFERENCES Wagon (WagonIDW));";
			stmt.executeUpdate(sql);

			sql = 	"CREATE TABLE IF NOT EXISTS Kunde " +
					"(KundenID INT PRIMARY KEY," +
					" Name TEXT,"+
					" Sitzplatz INT UNIQUE ,"+
					" FOREIGN KEY (Sitzplatz) REFERENCES Wagon (SitzplatzW));";
			stmt.executeUpdate(sql);

			sql = "insert into Wagon values (1, 10);";
			stmt.executeUpdate(sql);

			sql = "insert into Wagon values (2, 20);";
			stmt.executeUpdate(sql);

			sql = "insert into WagonKlasse values (1, 1);";
			stmt.executeUpdate(sql);

			sql = "insert into WagonKlasse values (2, 2);";
			stmt.executeUpdate(sql);

			sql = "insert into Kunde values (1, Horst, 10);";
			stmt.executeUpdate(sql);

			sql = "insert into Kunde values (2, Kurt, 20);";
			stmt.executeUpdate(sql);
			
			sql = "select  name,  sitzplatz from  kunden  where  sitzplatz IN  (select sitzplatz ID  from  sitz2Waggon  wherewaggonID IN  ( select  waggonID  from  waggonklassen  where  klassenbezeichnung = "+" 1"+" ) ) ;";
			stmt.executeUpdate(sql);
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
